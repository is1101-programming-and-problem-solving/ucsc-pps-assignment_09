// SALU Dissanayake - 20020325
// IS1101 Programming and Problem Solving
// Assignment 09
// File Handling

#include <stdio.h>

int main()
{
    FILE *fptr;

    fptr = fopen("assignment9.txt", "w");
    fprintf(fptr, "UCSC is one of the leading institutes in Sri Lanka for computing studies.");
    fclose(fptr);

    fptr = fopen("assignment9.txt", "r");
    char str1[75];
    fgets(str1, 75, fptr);
    printf("%s\n", str1);
    fclose(fptr);
    
    fptr = fopen("assignment9.txt", "a");
    fprintf(fptr, "\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
    fclose(fptr);

    return 0;
}
