# UCSC PPS Assignment_09

**File Handling in C**

Create a file named "assignment9.txt" and open it in write mode. Write the sentence "UCSC is one of the leading institutes in Sri Lanka for computing studies." to the file.<br>
Open the file in read mode and read the text from the file and print it to the output screen.<br>
Open the file in append mode and append the sentence "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields." to the file.

[_Click here to view the code._](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_09/-/blob/master/PPS_A9_fileHandling.c)<br>
[_Click here to view the txt file._](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_09/-/blob/master/assignment9.txt)
